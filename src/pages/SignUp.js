import React, {Component} from 'react';
import{
    StyleSheet,
    Text,
    View,
    TextInput,
    TouchableOpacity,
} from 'react-native';

import Logo from '../components/Logo';
import Form from '../components/Form';

import {Actions} from 'react-native-router-flux';

export default class Signup extends Component {
    goBack() {
        Actions.pop();
    }
    render() {
        return(
            <View style={styles.container}>
                <Logo/>
                <Form type="Sign Up">
                    <TextInput style={styles.inputBox} underlineColorAndroid='rgba(0,0,0,0)' placeholder="Re-Enter Password" secureTextEntry={true} placeholderTextColor = "#fff" ref={(input) => this.password = input}/>
                </Form>
                <View style={styles.signupTextCont}>
                    <Text style={styles.signupText}>Already have an account?</Text>
                    <TouchableOpacity onPress={this.goBack}><Text style={styles.signupButton}> Sign In</Text></TouchableOpacity>
                </View>
            </View>
        )
        }
    }

const styles = StyleSheet.create({
    container : {
        backgroundColor:'#455a64',
        flex: 1,
        alignItems:'center',
        justifyContent :'center'
    },
    signupTextCont : {
        flexGrow: 1,
        alignItems:'flex-end',
        justifyContent :'center',
        paddingVertical:16,
        flexDirection:'row'
    },
    signupText: {
        color:'rgba(255,255,255,0.6)',
        fontSize:16
    },
    signupButton: {
        color:'#ffffff',
        fontSize:16,
        fontWeight:'500'
    },
    inputBox: {
        width: 300,
        backgroundColor: "rgba(255, 255, 255, 0.2)",
        borderRadius: 25,
        paddingHorizontal: 16,
        fontSize: 16,
        color: "#fff",
        marginVertical: 10
    }
});
