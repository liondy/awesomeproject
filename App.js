/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component,Fragment,useEffect} from 'react';
import {
  StyleSheet,
  View,
  Text,
  StatusBar,
  Image,
  Platform
} from 'react-native';

import Routes from "./src/Routes";
import SplashScreen from 'react-native-splash-screen';

export default class App extends Component{
  render(){
    useEffect(() => {
      SplashScreen.hide();
    },[]);
    return (
      <Fragment>
        {Platform.OS==='ios'&&<StatusBar barStyle="dark-content"/>}
        <View style={styles.sectionContainer}>
          <StatusBar backgroundColor= "#1c313a" barStyle= "light-content"/>
          <Text>Hello World</Text>
          <Routes/>
        </View>
      </Fragment>
    )
  }
}

const styles = StyleSheet.create({
  sectionContainer: {
    flex: 1,
  },
});

